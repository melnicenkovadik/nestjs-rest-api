import {Module} from "@nestjs/common";
import {ProductsController} from "./products.controller";
import {ProductsService} from "./dto/products.service";

@Module({
    providers: [ProductsService],
    controllers: [ProductsController]
})
export class ProductsModule {
}